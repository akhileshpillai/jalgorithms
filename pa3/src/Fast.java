import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/*************************************************************************
 * Name: Akhilesh Pillai
 * Email: akhileshpillai@gmail.com
 *
 *
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for Pointints in the plane.
 *
 *************************************************************************/


public class Fast {
	
	private static void merge(Point pnt,Point[] a, Point[] aux, int lo, int mid, int hi){
		int i = lo;
		int j = mid + 1;
		for (int k = lo; k <= hi; k++){
			if (i > mid)	aux[k] = a[j++];
			else if (j > hi) aux[k] = a[i++];
			else if (pnt.SLOPE_ORDER.compare(a[j],a[i]) == -1) aux[k] = a[j++];
			else aux[k] = a[i++];
		}
	}
	
	private static void sort(Point pnt, Point[] a, Point[] aux, int lo, int hi){
		if (hi <= lo) return;
		int mid = lo + (hi - lo) / 2;
		sort (pnt,aux,a, lo, mid);
		sort (pnt,aux,a, mid + 1 , hi);
		merge(pnt,aux,a,lo,mid,hi);
	}
	
	private static Point minPt(Point[] ans){
		Point min = ans[0];
		for (Point p: ans){
			if (p.compareTo(min) == -1){
				min = p;
			} 
		}
		return min;
	} 
	
    // unit test
    public static void main(String[] args) {
    	/* YOUR CODE HERE */
    	
    	In input = new In(args[0]);
   
    	//rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);	
    	
        int N = input.readInt();
    	
    	Point[] pts = new Point[N];
    	Point[] aux = new Point[N];
    	Point[] copy_pts = new Point[N];
    	Point[] ans; 
    	int ans_count = 0;
    	
    	for(int i=0;i<N;i++){
    		int x = input.readInt();
    		int y = input.readInt();
    		pts[i] = new Point(x,y);
    		copy_pts[i] = pts[i];
    	}
    
    	Out output = new Out();
    	HashMap<String,Point[]> hmap = new HashMap<String, Point[]>();
    	
    	for(int i=0;i<N;i++){
    		for(int copy=0; copy<N; copy++){
    			aux[copy]=copy_pts[copy];
    			pts[copy]=copy_pts[copy];
    		}
    		
    		sort(aux[i], aux, pts, 0, N-1);
    		
    		
    		
    		for(int j=0;j<N-2;j++){
    			int count = 0;
    			int k=j;
    			while ((aux[0].slopeTo(aux[k]) == aux[0].slopeTo(aux[j+1])) && (j<N-2)){
    				count++;
    				j+=1; 
    			}
    			if (count >=2){
    				aux[0].drawTo(aux[k]);
    				aux[k].drawTo(aux[j]);
    				ans_count = 0;
    				ans = new Point[j - k + 2];
    				ans[ans_count++]= aux[0];
    				
    				
    				for(int nc=k; nc<=j; nc++){
    					
    				 ans[ans_count++] = aux[nc];
    					//output.print(aux[nc].toString());
    				
    				}
    				Point min_pt = minPt(ans);
    				String key = aux[0].slopeTo(aux[k]) + min_pt.toString();
    				hmap.put(key, ans);
    				
    			}
    		}
    		
    	
    	}
    	for(Point[] val: hmap.values()){
    		output.println();
    		Arrays.sort(val);
    		for(Point p: val){
    			output.print(p.toString());
    		}
    		
    	}
    	
    	StdDraw.show(0);
    } //main
}//fast
    	
