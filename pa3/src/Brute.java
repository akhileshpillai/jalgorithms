/*************************************************************************
 * Name: Akhilesh Pillai
 * Email: akhileshpillai@gmail.com
 *
 *
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for Pointints in the plane.
 *
 *************************************************************************/


public class Brute {

    // unit test
    public static void main(String[] args) {
    	/* YOUR CODE HERE */
    	In input = new In(args[0]);
   
    	// rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);	
    	
        int N = input.readInt();
    	
    	Point[] pts = new Point[N];
    	
    	for(int i=0;i<N;i++){
    		int x = input.readInt();
    		int y = input.readInt();
    		pts[i] = new Point(x,y);
    		pts[i].draw();
    	}
    	
    	
    	Out output = new Out();
    	for(int i=0;i<N;i++){
    		for(int j=i+1;j<N;j++){
    			double ij = pts[i].slopeTo(pts[j]);
    			for(int k=j+1;k<N;k++){
    				double ik = pts[i].slopeTo(pts[k]);
    				for(int l=k+1; l<N ; l++){
    					double il = pts[i].slopeTo(pts[l]);
    					if ((ij == ik) && (ik == il)){
    						output.println("ij : " + ij + " ik: " + ik + " il: " + il );
    						output.println(pts[i].toString() + "-->" + pts[j].toString() + "-->" + pts[k].toString() + "-->" + pts[l].toString());
    						pts[i].drawTo(pts[j]);
    						pts[i].drawTo(pts[k]);
    						pts[i].drawTo(pts[l]);
    					}
    				}
    			}
    		}
    		
       	}
    	StdDraw.show(0);
    }

}
