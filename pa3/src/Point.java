/*************************************************************************
 * Name:
 * Email:
 *
 * Compilation:  javac Point.java
 * Execution:
 * Dependencies: StdDraw.java
 *
 * Description: An immutable data type for points in the plane.
 *
 *************************************************************************/

import java.util.Comparator;

public class Point implements Comparable<Point> {

    // compare points by slope
    public final Comparator<Point> SLOPE_ORDER = new Slope_Compare();       // YOUR DEFINITION HERE

    private final int x;                              // x coordinate
    private final int y;                              // y coordinate

    // create the point (x, y)
    public Point(int x, int y) {
        /* DO NOT MODIFY */
        this.x = x;
        this.y = y;
    }

    // plot this point to standard drawing
    public void draw() {
        /* DO NOT MODIFY */
        StdDraw.point(x, y);
    }

    // draw line between this point and that point to standard drawing
    public void drawTo(Point that) {
        /* DO NOT MODIFY */
        StdDraw.line(this.x, this.y, that.x, that.y);
    }

    // slope between this point and that point
    public double slopeTo(Point that) {
        /* YOUR CODE HERE */
    	
    	double yaxis = that.y - this.y;
    	double xaxis = that.x - this.x;
    	
    	if ((yaxis == 0) &&  (xaxis != 0)){
    		return +0.0;
    	}
    	else if ((yaxis == 0) &&  (xaxis == 0)){
    		return Double.NEGATIVE_INFINITY;
    	}
    	else if((yaxis != 0) &&  (xaxis == 0)){
    		return Double.POSITIVE_INFINITY;
    	}
    	else
    	{
    		return yaxis/xaxis;
    	}
    	
    }
    
    // is this point lexicographically smaller than that one?
    // comparing y-coordinates and breaking ties by x-coordinates
    public int compareTo(Point that) {
		if ((this.y < that.y) || ((this.y == that.y) && (this.x < that.x))) {
			return -1;
		}
		else if ((this.y > that.y) || ((this.y == that.y) && (this.x > that.x))) {
			return 1;
		}
		else 
			return 0;
    }

    // return string representation of this point
    public String toString() {
        /* DO NOT MODIFY */
        return "(" + x + ", " + y + ")";
    }
    
    //private class comparator that implements compare method.
    
    private class Slope_Compare implements Comparator<Point>{

		@Override
		public int compare(Point p1, Point p2) {
			if (Point.this.slopeTo(p1) > Point.this.slopeTo(p2)){
			
				return 1;
			}
			else if (Point.this.slopeTo(p1) < Point.this.slopeTo(p2)){
				return -1;
			}
			
			return 0;
		}
    	
    }
    

    // unit test
    public static void main(String[] args) {
    	/* YOUR CODE HERE */
    	In input = new In(args[0]);
   
    	// rescale coordinates and turn on animation mode
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        StdDraw.show(0);	
    	
        int N = input.readInt();
    	
    	Point[] pts = new Point[N];
    	
    	for(int i=0;i<N;i++){
    		int x = input.readInt();
    		int y = input.readInt();
    		pts[i] = new Point(x,y);
    	}
    	
    	Out output = new Out();
    	for(int i=0;i<N;i++){
    		for(int j=i+1;j<N;j++){
    			double ij = pts[i].slopeTo(pts[j]);
    			for(int k=j+1;k<N;k++){
    				double ik = pts[i].slopeTo(pts[k]);
    				for(int l=k+1; l<N ; l++){
    					double il = pts[i].slopeTo(pts[l]);
    					if ((ij == ik) && (ik == il)){
    						output.println("ij : " + ij + " ik: " + ik + " il: " + il );
    						output.println(pts[i].toString() + "-->" + pts[j].toString() + "-->" + pts[k].toString() + "-->" + pts[l].toString());
    						pts[i].drawTo(pts[j]);
    						pts[i].drawTo(pts[k]);
    						pts[i].drawTo(pts[l]);
    					}
    				}
    			}
    		}
    		
       	}
    	StdDraw.show(0);
    }

}
