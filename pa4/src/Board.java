/**
 * 
 */
import java.util.*;
/**
 * @author akhileshpillai
 *
 */
public class Board {
    private int N;
    private int[][] bd;
    private Board prev_sn;
    private int xzero,yzero;
    private Stack<Board> bd_neigh = new Stack<Board>();
    
    private void setBd(int[][] blocks){
    	
    	/*bd = Arrays.copyOf(blocks, N); 
    		another way to copy arrays.
		*/
    	bd = new int[N][N];
		for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){		
					this.bd[i][j] = blocks[i][j];
					if (bd[i][j] == 0){
						xzero = i;
						yzero = j;
					}
			}
		}
    }
    
    private final int[][] goalblock;
    
    private static int[][] getGoal(int n){
    	int element=0;
    	int[][] result = new int[n][n];
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				if ((i==n-1) && (j==n-1)){
					result[i][j] = 0;
				}
				else{
					result[i][j] = ++element;
				}
			}
		}
		return result;
    }
	
    public Board(int[][] blocks){
	/* construct a board from an N-by-N array of blocks 
	 * (where blocks[i][j] = block in row i, column j)
	 */
		this.N = blocks.length;
		this.setBd(blocks);
		this.goalblock = getGoal(blocks.length);
    }
    
    public int dimension() {
    	/*   board dimension N  */
    	return N;
    }            
    
    public void printBoard(){
     /* to be deleted */
    	for(int i=0;i<N;i++){
    		StdOut.println();
			for(int j=0;j<N;j++){
				StdOut.print(goalblock[i][j]);
				
			
		}
      } 
    }
      
    public int hamming(){
    	int ham_distance = 0;
    	for(int i=0;i<N;i++){
    		for(int j=0;j<N;j++){
    			if( (bd[i][j] != 0) && (bd[i][j] != goalblock[i][j])){
    				ham_distance++;
    			}
    			
    		}
    	}
    	return ham_distance;
    }                   // number of blocks out of place
    
    public int manhattan(){
    	int man_distance=0;
    	for(int i=0;i<N;i++){
    		for(int j=0;j<N;j++){
    			for(int k=0;k<N;k++){
    	    		for(int l=0;l<N;l++){
    	    			if((bd[i][j] != 0) && (bd[i][j] == goalblock[k][l])){
    	    				man_distance = man_distance + Math.abs(i-k) + Math.abs(j-l);
    	    			}
    			
    	    		}
    			}
    		}
    	}
    	
    	return man_distance;
    }                 // sum of Manhattan distances between blocks and goal
    
    public boolean isGoal(){
    	return Arrays.deepEquals(this.bd, this.goalblock);
    }                // is this board the goal board?
    
   /* public Board twin()  {
    	
    }                  // a board obtained by exchanging two adjacent blocks in the same row
   */
    public boolean equals(Object y){
    	if (y == this) return true;
    	if (y == null) return false;
    	if (y.getClass() != this.getClass()) return false;
    	Board that = (Board) y;
    	return Arrays.deepEquals(this.bd, that.bd);
    } // does this board equal y?
    
    private int[][] find_neigh(int x, int y){
    	int [][] nbour = new int[N][N];
    	for(int i=0;i<N;i++){
			for(int j=0;j<N;j++){		
					nbour[i][j] = bd[i][j];			
			}
		}
    	
    	if (!((x < 0) || (y < 0) || (x > N - 1) || (y > N-1))){
    		nbour[xzero][yzero] = nbour[x][y];
    		nbour[x][y] = 0;
    	}
    	return nbour;
    }
    
 
    private void updateStack(int [][] ustemp){
    	Board tempBd;
    	if (!Arrays.deepEquals(bd, ustemp)){
    		tempBd = new Board(ustemp);
    		bd_neigh.push(tempBd);
    		
    	}
    	
  }
    
    
    public Iterable<Board> neighbors() {
    	
    	int [][] temp;
    	temp = find_neigh(xzero -1 , yzero);
    	updateStack(temp);
    	
    	temp = find_neigh(xzero + 1, yzero);
    	updateStack(temp);
    	
    	temp = find_neigh(xzero, yzero - 1);
    	updateStack(temp);
    	
    	temp = find_neigh(xzero, yzero + 1);
    	updateStack(temp);
    	
    	return bd_neigh;
    }    // all neighboring boards
    
    public String toString(){
    	StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        s.append(String.format("Manhattan : %2d \n",this.manhattan()));
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                s.append(String.format("%2d ", bd[i][j]));
            }
            s.append("\n");
        }
        return s.toString();
    	
    }               // string representation of the board (in the output format specified below)
   
}
