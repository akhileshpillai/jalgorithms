/**
 * 
 */

/**
 * @author akhileshpillai
 *
 */
public class Solver {
	

	/**
	 * @param args
	 */
	private MinPQ<SearchNode> pq = new MinPQ<SearchNode>();
	
	private int num_moves = 0;
	
	private SearchNode sol_node;
	private Stack<Board> solution_bds = new Stack<Board>();
	
	public Solver(Board initial) {
		SearchNode init = new SearchNode();
		init.prev = null;
		init.snBoard = initial;
		init.setNum_of_moves(num_moves);
		
		
		pq.insert(init);

	}           // find a solution to the initial board (using the A* algorithm)
    private class SearchNode implements Comparable<SearchNode>{

    	private Board snBoard;
    	private int num_of_moves;
    	private SearchNode prev;
		
		@Override
		public int compareTo(SearchNode o) {
			// TODO Auto-generated method stub
			return (this.num_of_moves + this.snBoard.manhattan()) - (o.num_of_moves +o.snBoard.manhattan());
		}

		public int getNum_of_moves() {
			return num_of_moves;
		}

		public void setNum_of_moves(int num_of_moves) {
			this.num_of_moves = num_of_moves;
		}
    	
    }
	
	
	public boolean isSolvable() { 
	
		
	    SearchNode temp,add_snode;
    	int npf,ppf;
    	ppf = 0;
	    while(!pq.isEmpty()){
    		
    		temp = pq.delMin();
    		if (temp.snBoard.isGoal()){
    			sol_node=temp;
    			return true;
    		}
    		
    			
    		num_moves = temp.num_of_moves;
    		
    		for (Board t: temp.snBoard.neighbors() ){
    			 
    			add_snode = new SearchNode();
    			add_snode.snBoard = t;
    			add_snode.setNum_of_moves(num_moves + 1);
    			add_snode.prev = temp;
    	
    			if (temp.prev == null) pq.insert(add_snode);
    			else
    			{
    				if(!(temp.prev.snBoard.equals(add_snode.snBoard))){
    					pq.insert(add_snode);
    				}
    				
    			}
    				
    		}
    		
    	}// is the initial board solvable?
    	return false;
	}
	public int moves() {
    	return sol_node.num_of_moves;
    }                     // min number of moves to solve initial board; -1 if no solution
    
	public Iterable<Board> solution() {
    	while(sol_node.prev != null){
    		solution_bds.push(sol_node.snBoard);
    		sol_node = sol_node.prev;
    		
    	}
    	return solution_bds;
	}       // sequence of boards in a shortest solution; null if no solution
	
	public static void main(String[] args) {
		// create initial board from file
	    In in = new In(args[0]);
	    int N = in.readInt();
	    int[][] blocks = new int[N][N];
	    for (int i = 0; i < N; i++)
	        for (int j = 0; j < N; j++)
	            blocks[i][j] = in.readInt();
	    Board initial = new Board(blocks);

	    // solve the puzzle
	    Solver solver = new Solver(initial);

	    // print solution to standard output
	    if (!solver.isSolvable())
	        StdOut.println("No solution possible");
	    else {
	        StdOut.println("Minimum number of moves = " + solver.moves());
	        for (Board board : solver.solution())
	           
	        	StdOut.println(board); 
	    }
	} 
	
}
