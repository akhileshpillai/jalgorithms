import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Test;


public class DequeTest {

	Deque<String> testdeque ;
	String first;
	String second;
	String last;
	@Before
	public void setUp() throws Exception {
		testdeque = new Deque<String>();
		
	}

	@Test
	public void testAddFirst() {
		testdeque.addFirst("one");
		testdeque.addFirst("two");
		testdeque.addFirst("three");
		assertTrue(testdeque.size() == 3);
		
		 
	}

	@Test
	public void testAddLast() {
		testdeque.addFirst("one");
		testdeque.addFirst("two");
		testdeque.addFirst("three");
		testdeque.addLast("four");
		assertTrue(testdeque.size() == 4);
	}

	@Test
	public void testRemoveFirst() {
		testdeque.addFirst("one");
		testdeque.addFirst("two");
		testdeque.addFirst("three");
		first = testdeque.removeFirst();
		second = testdeque.removeFirst();
		assertEquals("three", first);
		assertEquals("two", second);
	}

	@Test
	public void testRemoveLast() {
		
		testdeque.addFirst("three");
		testdeque.addLast("four");
		last = testdeque.removeLast();
		assertTrue(last.equals("four"));
		testdeque.addFirst("five");
		assertTrue(testdeque.size() == 2);
		last = testdeque.removeLast();
		assertTrue(last.equals("three"));
	}

	@Test
	public void testRemoveFirstNull() {
		try
		{
			testdeque.addFirst("one");
			testdeque.addFirst("two");
			testdeque.addFirst("three");
			first = testdeque.removeFirst();
			first = testdeque.removeFirst();
			first = testdeque.removeFirst();
			first = testdeque.removeFirst();
			fail("You are a monkey");
		}
		catch (NoSuchElementException e)
		{
		    System.out.println("there was an : " + e);
		}
	}
	
	@Test
	public void testRemoveLastNull() {
		try
		{
			first = testdeque.removeLast();
			fail("You are a monkey");
		}
		catch (NoSuchElementException e)
		{
		    System.out.println("there was an : " + e);
		}
	}
	@Test
	public void testaddFirstNull() {
		try
		{
			 testdeque.addFirst(null);
			 fail("You are a monkey");
		}
		catch (NullPointerException e)
		{
		    System.out.println("there was an : " + e);
		}
	}
	
	@Test
	public void testiterator(){
		testdeque.addFirst("one");
		testdeque.addFirst("two");
		testdeque.addFirst("three");
		testdeque.addFirst("four");
		testdeque.addLast("five");
		testdeque.addLast("six");
		testdeque.addLast("seven");
		testdeque.addLast("eight");
		testdeque.addLast("nine");
		
		for (String s : testdeque){
			StdOut.println(s);
		}
	}
}
