import java.util.Iterator;
import java.util.NoSuchElementException;


/**
 * 
 */

/**
 * @author akhileshpillai
 *
 */


	
public class RandomizedQueue<Item> implements Iterable<Item> {

	private Item[] a;         // array of items
	private int N;            // number of elements on stack

	// create an empty stack
	public RandomizedQueue() {
	        a = (Item[]) new Object[2];
	    } // construct an empty randomized queue
	
	public boolean isEmpty() { return N == 0;}          // is the queue empty?
	
	public int size(){ return N; }                 // return the number of items on the queue
	
	public void enqueue(Item item){
		if (item == null){
			throw new NullPointerException();
		}
		if ( N == a.length ) resize(2 * a.length);
		a[N++] = item;
		
	}     // add the item
	
	// resize the underlying array holding the elements
    private void resize(int capacity) {
        assert capacity >= N;
        Item[] temp = (Item[]) new Object[capacity];
        for (int i = 0; i < N; i++) {
            temp[i] = a[i];
        }
        a = temp;
    }
	
    public Item dequeue(){
    	if (isEmpty()) throw new NoSuchElementException("Queue underflow");
    	// Random r = new Random();
    	
    	int index = StdRandom.uniform(N);
    	Item pitem = a[index];
    	a[index] = a[N-1];
    	a[N-1] = null;
    	N--;
    	if ( N > 0 && N == a.length/4 ) resize((a.length / 2));
    	return pitem;
    }              // delete and return a random item
	
	public Item sample(){
		if (isEmpty()) throw new NoSuchElementException("Queue underflow");
    	int index = StdRandom.uniform(N); 
    	return a[index];
    	
	}               // return (but do not delete) a random item
	
	public Iterator<Item> iterator() { return new RandomizedQueueIterator(); } // return an independent iterator over items in random order
		
	private class RandomizedQueueIterator implements Iterator<Item>{
			
			private int i;
			
			private Item[] temp;
			public RandomizedQueueIterator(){
				i = N;
				temp = (Item[]) new Object[N];
			        for (int i = 0; i < N; i++) {
			            temp[i] = a[i];
			    }
			}
			
			@Override
			public boolean hasNext() {
				
				return i > 0;
			}

			@Override
			public Item next() {
				if (!this.hasNext()) throw new NoSuchElementException("Queue underflow");
				
		    	int index =  StdRandom.uniform(i);
		    	
		    	Item pitem = temp[index];
		    	temp[index] = temp[i-1];
		    	temp[i-1] = null;
		    	i--;
		    	return pitem;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
				
			}
		}
		
	 

}
	


