import static org.junit.Assert.*;

import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;


public class RandomizedQueueTest {

	RandomizedQueue<String> rq;
    String eone;
    String rone;
    String rtwo;
    String rthree;

	@Before
	public void setUp() throws Exception {
      rq = new RandomizedQueue<String>();
      
	}

	@Test
	public void testdeque() {
		rq.enqueue("akhilesh");
		rq.enqueue("sriraga");
		rq.enqueue("has a");
		rone = rq.dequeue();
		assertEquals(2,rq.size());
		rtwo = rq.dequeue();
		assertEquals(1,rq.size());
		rthree = rq.dequeue();
		assertEquals(0,rq.size());
		
	}
	
	@Test
	public void testenque() {
		rq.enqueue("akhilesh");
		rq.enqueue("sriraga");
		rq.enqueue("has a");
		assertTrue(rq.size() == 3);
	}
	@Test
	public void testsample() {
		rq.enqueue("akhilesh");
		rq.enqueue("sriraga");
		rq.enqueue("has a");
		rone = rq.sample();
		assertTrue(rq.size() == 3);
		
	}
	
	@Test
	public void testdequeNull() {
		try
		{
			String rone = rq.dequeue();
			fail("You are a monkey");
		}
		catch (NoSuchElementException e)
		{
		    System.out.println("there was an : " + e);
		}
	}
	@Test
	public void testenqueNull() {
		try
		{
			 rq.enqueue(null);
			 fail("You are a monkey");
		}
		catch (NullPointerException e)
		{
		    System.out.println("there was an : " + e);
		}
	}
	
	@Test
	public void testiterator(){
		rq.enqueue("akhilesh");
		rq.enqueue("sriraga");
		rq.enqueue("has a");
		rq.enqueue("lesh");
		rq.enqueue("raga");
		rq.enqueue("haa");
		
		for (String s : rq){
			StdOut.println(s);
		}
	}

}
