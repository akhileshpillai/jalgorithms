
public class Subset {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int k = Integer.parseInt(args[0]);
		int counter = 1;

		RandomizedQueue<String> rq = new RandomizedQueue<String>();

		while (!StdIn.isEmpty()) {
			String item = StdIn.readString();
			rq.enqueue(item);
		}

		System.out.println("\n");
		
		while (counter <= k) {
			
			System.out.println(rq.dequeue());
			counter++;
		}
	}

}
