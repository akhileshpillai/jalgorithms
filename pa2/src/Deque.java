/**
 * 
 */

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author akhileshpillai
 * 
 */
public class Deque<Item> implements Iterable<Item> {
	private Node first;
	private Node last;
	
	
	private class Node {
		Item item;
		Node next;
		Node prev;
	}
	
	private int itemcount;
	
	public Deque() {
		
		first = null;
		last = null;
		itemcount = 0; 
		
	} // construct an empty deque

	public boolean isEmpty() {
		if (this.size() == 0) {
			return true;
		} else {
			return false;
		}

	} // is the deque empty?

	public int size(){
	  return itemcount;
   }                  // return the number of items on the deque

	public void addFirst(Item item) {
		Node frontnode = new Node();
		frontnode.item = item;
		if (item == null){
			throw new NullPointerException();
		}
		if (this.isEmpty()){			
			frontnode.next = null;
			frontnode.prev = null;
			first = frontnode;
			last = frontnode;
		} else {
			frontnode.next = first;
			frontnode.prev = null;
			first.prev = frontnode;
			first = frontnode;
		}
		++itemcount;
		
	} // insert the item at the front

	public void addLast(Item item) {
		
		Node endnode = new Node();
		endnode.item = item;
		if (item == null){
			throw new NullPointerException();
		}	
		
		if (this.isEmpty()){	
			endnode.prev = null;
			endnode.next = null;
			first = endnode;
			last = endnode;
		} else{
			endnode.prev = last;
			endnode.next = null;
			last.next = endnode;
			last= endnode;
		}	
		
		++itemcount;
		
	} // insert the item at the end

	public Item removeFirst() {
		if (this.isEmpty()){
			throw new NoSuchElementException();
		}
		Item rf = first.item;
		if (this.size() == 1){
			first = null;
			last = null;
		}else{
			first = first.next;
			first.prev = null;
		}
		--itemcount;
		
		return rf;
		
	} // delete and return the item at the front

	public Item removeLast() {
		if (this.isEmpty()){
			throw new NoSuchElementException();
		}
		Item rl = last.item;
		if (this.size() == 1){
			last = null;
			first = null;
			
		}else{
			last = last.prev;
			last.next = null;
		}
		--itemcount;
		return rl;
		
	} // delete and return the item at the end

	public Iterator<Item> iterator(){ return new dequeIterator();}
		
	private class dequeIterator implements Iterator<Item>{
		private Node current = first;	
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			
			return (current != null);
		}

		@Override
		public Item next() {
			if (!this.hasNext()) throw new NoSuchElementException("Queue underflow");
			Item item = current.item;
			current = current.next;
			// TODO Auto-generated method stub
			return item;
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException();
			
		}

			
		
		
	} // return an iterator over items in order
										// from front to end

	

}
